# Host Machine IaC

```bash
sudo visudo
```

Add this line:

```txt
# Allow members of the group sudo to execute apt without a password
%sudo ALL=NOPASSWD:/usr/bin/apt
```

Test from a `Command Prompt`, validate no password is required.

``` cmd
%windir%\system32\bash.exe -c "sudo apt update && sudo apt -y upgrade"
```

Create the task in a Windows `Command Prompt`:

``` cmd
schtasks /create /xml Task-LinuxUpgrade.xml /tn UpdateLinux
```

Test the schedule works:

```cmd
schtasks /run /i /tn "UpdateLinux"
```

## References

- [Automating Updates For Bash On Ubuntu On Windows 10](http://www.riosec.com/articles/automatingupdatesforbashonubuntuonwindows10)
